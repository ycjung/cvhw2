load('model');
consts;
confusion = zeros(size(CLASSES, 2));

labels = [];
hogs = [];

for i=1:size(CLASSES, 2)
    data = load_image(i, CLASSES(i), TRAIN_SIZE+1, TEST_SIZE, IMAGE_WIDTH, IMAGE_WIDTH);
    
    b_labels = [data.class];
    b_labels = double(b_labels(:));
    b_hogs = [];
    for j = 1:(size(data, 2))
        bw = data(j).bws;
        hog = feature_hog(bw, CELL_WIDTH, BLOCK_WIDTH, BIN_DIM);
        b_hogs = [b_hogs; hog];
    end
    labels = [labels; b_labels];
    hogs = [hogs; b_hogs];
end
hogs = double(hogs);
[prediction, acc, ~] = predict(labels, sparse(hogs), model);

for i=1:size(labels, 1)
    row = labels(i);
    col = prediction(i);
    confusion(row, col) = confusion(row, col) + 1;
end

confusion