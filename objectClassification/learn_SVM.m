function learn_SVM()
    consts;
    % Load train images and feed to SVM. generate a SVM file.
    labels = [];
    hogs = [];
    for i=1:size(CLASSES, 2)
        CLASSES(i)
        data = load_image(i, CLASSES(i), 1, TRAIN_SIZE, IMAGE_WIDTH, IMAGE_WIDTH);
        b_labels = [data.class];
        b_labels = double(b_labels(:));
        b_hogs = [];
        for j = 1:(size(data, 2))
            bw = data(j).bws;
            hog = feature_hog(bw, CELL_WIDTH, BLOCK_WIDTH, BIN_DIM);
            b_hogs = [b_hogs; hog];
        end
        labels = [labels; b_labels];
        hogs = [hogs; b_hogs];
    end
    hogs = double(hogs);
    model = train(labels, sparse(hogs)); %liblinear internally uses 1-vs-rest.
    
    save('model', 'model');
end
