CLASSES = {'car_side', 'airplanes', 'ferry', 'elephant', 'chair'};
CELL_WIDTH = 5;
BLOCK_WIDTH = 2;
BIN_DIM = 9;
IMAGE_WIDTH = 256;

TRAIN_SIZE = 30;
TEST_SIZE = 20;