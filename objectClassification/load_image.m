function images = load_image(label, class_name, start, count, width, height)
    images = [];

    folder = ['dataset/', char(class_name), '/'];
    files = dir(folder);
    names = {files(~[files.isdir]).name};
    names = names(start:start+count-1)
    paths = cellfun(@(x) [folder, x], names, 'UniformOutput', false);

    f = @(x) imresize(sum(imread(x), 3) ./3 ./255, [width height]);
    bw = cellfun(f, paths, 'UniformOutput', false);
    images = [images; struct('class', double(label), 'bws', bw)];
end

