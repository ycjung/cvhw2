function y = feature_hog( image, cell_width, block_width, bin_dim )
%build hog feature
    y = extractHOGFeatures(image, 'CellSize', [cell_width cell_width], 'BlockSize', [block_width block_width], 'NumBins', bin_dim); 
    
    % extractHogFeature uses L2-norm normalization internally, so, we can
    % directly apply clipping to achieve L2-hys normalization on the result
    y = max(y, 0.2) * 5;
end

