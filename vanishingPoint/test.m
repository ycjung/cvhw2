IMAGE1_NAME = 'image1.jpg';
IMAGE2_NAME = 'image2.jpg';

image1 = sum(imread(IMAGE1_NAME), 3)./3 ./255;
canny1 = edge(image1, 'Canny', 0.2, 5);
image2 = sum(imread(IMAGE2_NAME), 3)./3 ./255;
canny2 = edge(image2, 'Canny', 0.2, 5);
figure();
subplot(1, 2, 1);
imshow(canny1);
subplot(1, 2, 2);
imshow(canny2)



figure();
subplot(1, 2, 1);
draw_vanishing_point(canny1, image1);
subplot(1, 2, 2);
draw_vanishing_point(canny2, image2);

function draw_vanishing_point(edgeimg, orig)
DIV_COUNT =30;
BIN_WIDTH = (size(edgeimg, 2)/DIV_COUNT);
BIN_HEIGHT = (size(edgeimg, 1)/DIV_COUNT);
GRID_X = size(edgeimg, 1) / DIV_COUNT;
GRID_Y = size(edgeimg, 2) / DIV_COUNT;
%img = flip(flip(edgeimg, 1), 2);  
img = edgeimg;
[H,T,R] = hough(img);
P = houghpeaks(H,4);
lines = houghlines(img,T,R,P);
%imshow(img)
imagesc([0, size(orig, 2)], [0, size(orig, 1)], orig);
colormap(gray);

for i=1:(size(lines, 2)-1) % Draw vanishing line
    rho = lines(i).rho;
    theta = lines(i).theta * pi / 180;
    a = cos(theta);
    b = sin(theta);
    x0 = a*rho;
    y0 = b*rho;
    x1 = x0 + 2000*b;
    y1 = y0 - 2000*a;
    x2 = x0 - 2000*b;
    y2 = y0 + 2000*a;

    line([x1, x2], [y1, y2],  'Color', 'g', 'LineWidth', 2), hold on;
    xlim([0 size(edgeimg, 2)])
    ylim([0 size(edgeimg, 1)])
end
x = 1; y = 1;
while x < size(edgeimg, 1) %Draw Grid
    line([2000, -2000], [x, x], 'Color', 'b', 'LineWidth', 0.1);
    x = x+GRID_X;
end
while y < size(edgeimg, 2)
    line([y, y], [2000, -2000], 'Color', 'b', 'LineWidth', 0.1);   
    y = y+GRID_Y;
end

% voting for vanishing point
binvotes = [];
for i=1:(size(lines, 2)-1)
    for j=(i+1):(size(lines, 2)-1)
        rho1 = lines(i).rho;
        theta1 = lines(i).theta * pi / 180;
        rho2 = lines(j).rho;
        theta2 = lines(j).theta * pi / 180;
        
        A = [cos(theta1) sin(theta1); cos(theta2) sin(theta2)];
        r = [rho1; rho2];
        if(rcond(A) > 1e-12) % non singular
            xy = A\r;
            x = xy(1);
            y = xy(2);
            binx = floor(x / BIN_WIDTH);
            biny = floor(y / BIN_HEIGHT);
            binvotes = [binvotes; binx biny];
        end
    end
end

[C, a, b] = unique(binvotes, 'rows');
votecount = zeros([size(binvotes, 1), 1]);
for i=1:size(a)
    binxy = binvotes(a(i), :);
    binx = binxy(1);
    biny = binxy(2);
    for i=1:size(binvotes, 1)
        if(binx == binvotes(i, 1) && biny == binvotes(i, 2))
            votecount(i) = votecount(i) + 1;
        end
    end
end

[~, ind] = max(votecount);
maxbin = binvotes(ind, :);

% draw vanishing point
rectangle('Position', [maxbin(1)*BIN_WIDTH, maxbin(2)*BIN_HEIGHT, BIN_WIDTH, BIN_HEIGHT], 'FaceColor',[0 .5 .5]);

hold off

end
